~~~
     __                      .__              
   _/  |______ _______  _____|__| ___________ 
   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
    |__| (____  /__|  /____  >__|\___  >__|   
              \/           \/        \/       

Copyright Cameron Newnham 2015-2016
~~~
## General ##
---
### Purpose ###

This library is intended for use with Grasshopper, an extension to the 3D software Rhinoceros 3D. 

It's primary purpose is to add the point cloud functionality already apparent in Rhinoceros 5 to Grasshopper, with the ambition of treating point clouds as another useful data type, rather than an exotic type. 

It's secondary purpose is to provide tools for creation and streaming of point clouds with common scanning techniques (Kinect V1, V2 and OpenNI2). This is made possible due to the speed increases afforded by the use of the point cloud class. For example, a full point cloud from a Kinect V2 is released in approximately 10-20ms on standard machines, which is more than enough for most purposes.

Where possible, this library is multithreaded.

### Grasshopper usage ###

* As a traditional grasshopper library. The point cloud type is compatible with transforms in Grasshopper (move,scale,rotate,transform etc.)
* As a hybrid; to be used in Grasshopper, with some modules written as ad-hoc C# components. Rhino.Geometry.PointCloud and the new GH_PointCloud are inter-operable. As such, point clouds can be generated via other means and cast to GH_PointCloud for display and modification in grasshopper.

### Alternatives ###
* **Firefly** offers an extensive suite of tools dealing with vision and image processing, including Kinect streaming. However, in some cases interpreting the stream as a point cloud is more useful (and often faster!).
* **Volvox** offers an extensive suite of tools dealing with Point Clouds, and if conventional use is your ambition it is very fully featured and highly recommended. 

## Tarsier ##
---
### Creation ###
* **Construct Point Cloud**: Points, Colours and Normals to Point Cloud.
* **Deconstruct Point Cloud**: Point Cloud to Points, Colours and Normals.
* **Fill Box**: Crudely but quickly generates a point cloud in a box.
* **Fill Mesh**: Brute force to fill a mesh with a point cloud.
* **Populate Mesh**: Crudely but quickly populates the surface of a mesh with a point cloud.
* **Random Vectors**: Generates vectors in random directions.

### Display ###
* **Preview Point Cloud**: Previews a point cloud with an arbitrary point size.

### Modify ###
* **Clip Point Cloud**: Uses a box to clip (contain) the point cloud.
* **Filter Point Cloud**: Removes part of the point cloud based on colour value.
* **Merge Point Cloud**: Joins multiple point clouds into one.
* **Point Cloud Subset**: Extracts a subset of the point cloud by indices.
* **Voxelize Point Cloud**: A persistent decay grid for rationalizing and reducing point clouds. Each component update is one tick.

### Searching ###
* **Construct KDTree**: Constructs a KD-Tree.
* **Sample KDTree**: Uses Multi-threading to sample a KD-Tree (nearest-neighbours search).
* **Field of Vision**: Reduces a list of points to only contain those within a specified angle of a normal vector.

## Tarsier Vision ##
---
* **Kinect 1 Depth Reader**: Streams (colour) point cloud data from a Kinect V1.
* **Kinect 2 Depth Reader**: Streams (colour) point cloud data from a Kinect V2.
* **OpenNI2 Depth Reader**: Streams point cloud data from an OpenNI device.

## Requirements ##
---
To use the vision libraries, their respective SDKs must be installed (Kinect SDK 1.8 for Kinect V1, Kinect SDK 2.0 for Kinect V2, and OpenNI2 for other compatible devices). Refer to the wiki for further information. For Intel RealSense Devices you will need to install the RealSense SDK (https://www.intelrealsense.com/developers/).

## JSON Config for Realsense ##
---
Has only been tested with Intel Realsense D435 but should work with all D400 series devices.

The Intel Realsense wrapper does not use ini config files, but instead requires a json file to be loaded.

You can download existing json files here: https://github.com/IntelRealSense/librealsense/wiki/D400-Series-Visual-Presets or use the intel realsense-viewer (https://software.intel.com/en-us/realsense/d400/get-started) to generate your own json files.

- Name JSON file "realsense.json" and place in the same directory as the the rs2driver.dll.


Uses a modified verison of the OpenNI2 Wrapper.
https://github.com/MSDRobotics/RealSenseOpenNI2Wrapper


## Licensing and Credits ##
---

### License ###
~~~
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
~~~
### Contributions ###

#### Cameron Newnham ####
* Primary Development

#### Gwyllim Jahn ####
* Original Java implementations of scanners
* Kinect V1 component
* General advice

#### Ryan Pennings ####
* Added Orbecc and Intel Realsense Drivers (with config loading support)

### References ###

#### Karl Sanford ####
Smoothing Kinect Depth Frames in Real-Time, http://www.codeproject.com/Articles/317974/KinectDepthSmoothing, The Code Project Open License (CPOL) 1.02