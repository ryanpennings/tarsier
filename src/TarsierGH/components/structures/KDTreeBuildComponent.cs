﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;
using Tarsier.Algorithms;

namespace Tarsier.Tools
{
    public class KDTreeBuildComponent : GH_Component
    {
        public KDTreeBuildComponent()
          : base("Construct KDTree", "KDConstruct",
              "Builds a KDTree representation of a point cloud.",
              "Tarsier", "Searching")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.secondary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The reference point cloud.", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("KDTree", "K", "The KDTree.", GH_ParamAccess.tree);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud pc = null;            
            if (!DA.GetData("Point Cloud", ref pc)) return;
            DA.SetData(0, SpatialTools.ConstructKDTree(pc));
        }



        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_constructkdtree;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{3EE135F0-EA9E-45CB-8C36-013F8A88CF34}"); }
        }
    }
}
