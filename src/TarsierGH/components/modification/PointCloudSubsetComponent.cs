﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;

namespace Tarsier.Tools
{
    public class PointCloudSubsetComponent : GH_Component
    {
        public PointCloudSubsetComponent()
          : base("Point Cloud Subset", "Cloud Subset",
              "Extracts a subset of a point cloud based on indices.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quinary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to clip.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Indices", "i", "The points to select from the cloud.", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The extracted subset of the point cloud.", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud cloud = new PointCloud();
            List<int> indices = new List<int>();
            if (!DA.GetData("Point Cloud", ref cloud)) return;
            if (!DA.GetDataList("Indices", indices)) return;

            var npc = new PointCloud();
            foreach (int i in indices)
            {
                var item = cloud[i];
                npc.Add(item.Location, item.Normal, item.Color);
            }
            
            DA.SetData("Point Cloud", new GH_PointCloud(npc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_pointcloudsubset;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{E6FE2620-0FF7-4FE6-9D57-6F1FFC35B19A}"); }
        }
    }
}
