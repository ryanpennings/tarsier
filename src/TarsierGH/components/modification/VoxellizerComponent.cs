﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using TarsierGH;
using System.Windows.Forms;

namespace Tarsier.Tools
{
    public class VoxellizerComponent : GH_Component
    {
        public VoxellizerComponent()
          : base("Voxellize Point Cloud", "Voxellize",
              "Snaps a point cloud to a voxel grid, allowing for decay over time.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.septenary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to voxellize.", GH_ParamAccess.item);
            pManager.AddBoxParameter("Bounding Box", "B", "The extents of the grid.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Cell Size", "S", "The size of the grid cells.", GH_ParamAccess.item);
            pManager[pManager.AddNumberParameter("Decay Rate", "D", "The rate at which the cells decay.", GH_ParamAccess.item, 1)].Optional = true;
            pManager.AddIntegerParameter("Maximum Value", "M", "The maximum value a cell can take.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Lower Threshold", "T", "The minimum value a cell must have to be retained.", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The cropped point cloud.", GH_ParamAccess.item);
            pManager.AddBoxParameter("Bounding Box", "B", "The adjusted bounding box.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Voxel Data", "D", "The voxel grid as float[x,y,z].", GH_ParamAccess.item);
        }

        bool reset = false;

        Transform xfm;
        Transform inv;

        float[,,] grid;
        int x, y, z;
        double cellSize;
        Box b;

        System.Threading.Tasks.ParallelOptions pOpt = new System.Threading.Tasks.ParallelOptions
        {
            MaxDegreeOfParallelism = System.Environment.ProcessorCount * 2
        };

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud cloud = new PointCloud();
            Box boundingBox = new Box(); ;
            double cs = 0;

            int minVal = 5;
            int maxVal = 20;
            double decay = 1;

            if (!DA.GetData("Point Cloud", ref cloud)) return;
            if (!DA.GetData("Bounding Box", ref boundingBox)) return;
            //if (!DA.GetData("Reset", ref reset)) return;
            if (!DA.GetData("Cell Size", ref cs)) return;
            DA.GetData("Decay Rate", ref decay);
            if (!DA.GetData("Maximum Value", ref maxVal)) return;
            if (!DA.GetData("Lower Threshold", ref minVal)) return;

            if (grid == null || reset || cs != cellSize)
            {
                cellSize = cs;
                reset = false;
                b = CreateEnvironment(cellSize, boundingBox, out x, out y, out z);
                xfm = Transform.Identity;
                xfm *= Transform.Scale(Point3d.Origin, 1 / cellSize);
                xfm *= Transform.Translation(-(b.X.Min + b.Plane.Origin.X), -(b.Y.Min + b.Plane.Origin.Y), -(b.Z.Min + b.Plane.Origin.Z));
                inv = Transform.Identity;
                xfm.TryGetInverse(out inv);

                grid = new float[x, y, z];
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        for (int k = 0; k < z; k++)
                        {
                            grid[i, j, k] = 0;
                        }
                    }
                }
            }
            else {
                System.Threading.Tasks.Parallel.For(0, x, pOpt, i => {
                    for (int j = 0; j < y; j++)
                    {
                        for (int k = 0; k < z; k++)
                        {
                            grid[i, j, k] = grid[i, j, k] >= decay ? (float) (grid[i, j, k] -  decay) : 0;
                        }
                    }
                });
            }

            cloud = (Rhino.Geometry.PointCloud) cloud.Duplicate();
            cloud.Transform(xfm);

            // Snap to grid (at scale)
            System.Threading.Tasks.Parallel.ForEach(cloud, pOpt, pt => {
                int px = (int)pt.X;
                int py = (int)pt.Y;
                int pz = (int)pt.Z;
                if (px > 0 && px < x && py > 0 && py < y && pz > 0 && pz < z)
                grid[px, py, pz] = grid[px, py, pz] >= maxVal ? maxVal : grid[px, py, pz] + 1;
            });


            PointCloud pc = new PointCloud();
            System.Threading.Tasks.Parallel.For(0, x, pOpt, i => {
                for (int j = 0; j < y; j++)
                {
                    for (int k = 0; k < z; k++)
                    {
                        float val = grid[i, j, k] - minVal;
                        if (val > 0)
                        {
                            if (val > maxVal - minVal) val = maxVal - minVal;
                            int col = (int)(val * 255 / (maxVal - minVal));
                            lock (pc)
                            {
                                pc.Add(new Point3d(i, j, k), Color.FromArgb(col, col, col));
                            }
                        }
                    }
                }
            });

            pc.Transform(inv);
            DA.SetData("Point Cloud", new GH_PointCloud(pc));
            DA.SetData("Voxel Data", grid);
            DA.SetData("Bounding Box", b);
        }

        public Box CreateEnvironment(double cellSize, Box b, out int xDim, out int yDim, out int zDim)
        {
            Box box = new Box(b.Plane, b.X, b.Y, b.Z);
            box.RepositionBasePlane(b.Center);
            //box.RepositionBasePlane(box.Center);

            xDim = (int)Math.Ceiling((double)box.X.Length / (double)cellSize);
            yDim = (int)Math.Ceiling((double)box.Y.Length / (double)cellSize);
            zDim = (int)Math.Ceiling((double)box.Z.Length / (double)cellSize);

            double xLen = xDim * cellSize;

            box.X = new Interval(-(xDim * cellSize) / 2, (xDim * cellSize) / 2);
            box.Y = new Interval(-(yDim * cellSize) / 2, (yDim * cellSize) / 2);
            box.Z = new Interval(-(zDim * cellSize) / 2, (zDim * cellSize) / 2);

            return box;
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_voxellisepointcloud;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{CE716676-0D14-4F9F-B50F-6FEADC87B794}"); }
        }

        #region ResetOption
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            var item0 = Menu_AppendItem(menu, "Clear", Menu_OptionToggled, true, false);
            item0.ToolTipText = "Clear the values stored in the grid.";
        }

        private void Menu_OptionToggled(object sender, EventArgs args)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            if (item.Text == "Clear")
            {
                item.Checked = false;
                reset = true;
            }
        }

        #endregion
    }
}
