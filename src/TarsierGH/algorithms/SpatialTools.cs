﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


using KDTree;
using Rhino.Geometry;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarsier.Algorithms
{
    public static class SpatialTools
    {
        public static KDTree<int> ConstructKDTree(PointCloud cloud)
        {
            KDTree<int> tree = new KDTree<int>(3);
            foreach (var p in cloud)
            {
                tree.AddPoint(new double[] { p.Location.X, p.Location.Y, p.Location.Z }, p.Index);
            }
            return tree;
        }


        public static List<int>[] KDTreeMultiSearch(List<Point3d> pts, KDTree<int> tree, List<double> range, int maxReturned)
        {
            List<int>[] indices = new List<int>[pts.Count];

            var rangePartitioner = Partitioner.Create(0, pts.Count, (int)Math.Ceiling(pts.Count / (double)Environment.ProcessorCount * 2));

            Parallel.ForEach(rangePartitioner, (rng, loopState) =>
            {
                for (int i = rng.Item1; i < rng.Item2; i++)
                {
                    var p = pts[i];
                    var r = range[i];
                    var ns = tree.NearestNeighbors(new double[] { p.X, p.Y, p.Z }, maxReturned, r * r);
                    var indList = ns.ToList();
                    indices[i] = indList;
                }
            });

            return indices;
        }

        public static List<int>[] KDTreeMultiSearch(List<Point3d> pts, PointCloud cloud, List<double> range, int maxReturned)
        {
            KDTree<int> tree = new KDTree<int>(3);
            foreach (var p in cloud)
            {
                tree.AddPoint(new double[] { p.Location.X, p.Location.Y, p.Location.Z }, p.Index);
            }

            return KDTreeMultiSearch(pts, tree, range, maxReturned);
        }

        public static List<int> KDTreeSingleSearch(Point3d pt, PointCloud cloud, double range, int maxReturned)
        {
            KDTree<int> tree = new KDTree<int>(3);
            foreach (var p in cloud)
            {
                tree.AddPoint(new double[] { p.Location.X, p.Location.Y, p.Location.Z }, p.Index);
            }

            return tree.NearestNeighbors(new double[] { pt.X, pt.Y, pt.Z }, maxReturned, range * range).ToList();
        }

        public static bool[] InFovMultiCheck(Point3d loc, Vector3d dir, double fovRad, List<Point3d> neighbours)
        {
            bool[] states = new bool[neighbours.Count];

            Parallel.ForEach(Partitioner.Create(0, states.Length), range => {
                for (int x = range.Item1; x < range.Item2; x++)
                    states[x] = IsInFov(loc, dir, fovRad, neighbours[x]);
            });

            return states;
        }

        public static bool IsInFov(Point3d loc, Vector3d dir, double fov, Point3d testPt)
        {
            return Vector3d.VectorAngle(dir, testPt - loc) < fov / 2;
        }

        public static IEnumerable<PointCloudItem> GetNeighbours(this PointCloud cloud, Point3d pt, double range, int maxReturned)
        {
            return KDTreeSingleSearch(pt, cloud, range, maxReturned).Select(x => cloud[x]);
        }
        
    }
}
