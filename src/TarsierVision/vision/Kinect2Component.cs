﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Microsoft.Kinect;
using System.Drawing;
using System.Windows.Forms;
using TarsierVision.Processing;

namespace Tarsier.Kinect
{
    public class Kinect2Component : GH_Component
    {
        public Kinect2Component()
          : base("Kinect 2 Depth Reader", "Depth",
              "Reads data from a kinect when prompted.",
              "Tarsier", "Vision")
        {
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Enable", "E", "Enable this component. This will turn the kinect on and begin monitoring.", GH_ParamAccess.item, false);
            pManager.AddBooleanParameter("Release", "R", "Release the latest frame. Connect a timer to enable iterative release.", GH_ParamAccess.item, false);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Point Cloud", "C", "The depth buffer as a point cloud.", GH_ParamAccess.item);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return TarsierVision.Properties.Resources.icon_kinectv2; }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{2CA0DC47-1887-442B-BB56-2A32B6423335}"); }
        }

        private KinectSensor sensor;

        private CoordinateMapper coordinateMapper = null;
        private MultiSourceFrameReader multiFrameSourceReader = null;

        private ushort[] depthFrameData = null;
        private byte[] colorFrameData = null;
        private ColorSpacePoint[] colorPoints = null;
        private uint bytesPerColor = 4;
        private uint bytesPerDepth;
        private CameraSpacePoint[] cameraPoints = null;

        private PointCloud currentPC = new PointCloud();

        bool depthSet = false;
        int minDepth = 0;
        int maxDepth = 8000;
        int count;

        int depthWidth;
        int depthHeight;

        int colorWidth;
        int colorHeight;

        int prevCount = -1;

        private bool filteringEnabled = false;
        private bool smoothingEnabled = false;

        AveragedSmoothing averagedSmoothing;
        private int smoothBufferSize = 10;

        FilteredSmoothing filteredSmoothing;
        private int innerBandThreshold = 1;
        private int outerBandThreshold = 3;

        private int cropping = 0;
        private int detail = 1;

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool enable = false;
            bool release = false;
            if (!DA.GetData("Enable", ref enable)) return;
            if (!DA.GetData("Release", ref release)) return;


            if (enable)
            {
                sensor = KinectSensor.GetDefault();
                if (!sensor.IsOpen)
                {
                    prevCount = -1;
                    count = 0;

                    multiFrameSourceReader = sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color);

                    FrameDescription depthFrameDescription = sensor.DepthFrameSource.FrameDescription;
                    FrameDescription colorFrameDescription = sensor.ColorFrameSource.FrameDescription;

                    multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_FrameArrived;

                    depthWidth = depthFrameDescription.Width;
                    depthHeight = depthFrameDescription.Height;
                    colorWidth = colorFrameDescription.Width;
                    colorHeight = colorFrameDescription.Height;

                    depthFrameData = new ushort[depthWidth * depthHeight];
                    colorPoints = new ColorSpacePoint[depthWidth * depthHeight];
                    cameraPoints = new CameraSpacePoint[depthWidth * depthHeight];

                    //bytesPerColor = colorFrameDescription.BytesPerPixel;
                    bytesPerDepth = depthFrameDescription.BytesPerPixel;

                    colorFrameData = new byte[colorWidth * colorHeight * bytesPerColor];

                    coordinateMapper = sensor.CoordinateMapper;
                    sensor.Open();

                    averagedSmoothing = new AveragedSmoothing(smoothBufferSize, depthWidth, depthHeight);
                    filteredSmoothing = new FilteredSmoothing(innerBandThreshold, outerBandThreshold, depthWidth, depthHeight);
                }
            }
            else if (!enable && sensor != null && sensor.IsOpen)
            {
                sensor.Close();
                multiFrameSourceReader.MultiSourceFrameArrived -= this.Reader_FrameArrived;
                multiFrameSourceReader.Dispose();
                multiFrameSourceReader = null;
                sensor = null;
            }

            if (enable && sensor != null && sensor.IsOpen && release)
            {
                if (count > prevCount)
                {
                    prevCount = count;

                    currentPC = new PointCloud();

                    for (int x = 0 + cropping; x < depthWidth-cropping; x+= detail)
                    {
                        for (int y = 0 + cropping; y < depthHeight - cropping; y+= detail)
                        {
                            int ind = x + y * depthWidth;

                            CameraSpacePoint p = this.cameraPoints[ind];
                            ColorSpacePoint c = this.colorPoints[ind];

                            // make sure the depth pixel maps to a valid point in color space
                            int colorX = (int)Math.Floor(c.X + 0.5);
                            int colorY = (int)Math.Floor(c.Y + 0.5);
                            byte r = 0; byte g = 0; byte b = 0;
                            if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                            {
                                int colorIndex = (int) (((colorY * colorWidth) + colorX) * bytesPerColor);
                                r = this.colorFrameData[colorIndex++];
                                g = this.colorFrameData[colorIndex++];
                                b = this.colorFrameData[colorIndex++];
                            }

                            ushort val = depthFrameData[ind];
                            if (!(val > maxDepth || val < minDepth))
                            {
                                if (!(Single.IsInfinity(p.X)) && !(Single.IsInfinity(p.Y)) && !(Single.IsInfinity(p.Z)))
                                {
                                    var col = Color.FromArgb((int)r, (int)g, (int)b);
                                    currentPC.Add(new Point3d(p.X, p.Y, p.Z), col);
                                }
                            }
                        }
                    }
                }
                DA.SetData("Point Cloud", currentPC);
            }
        }

        private bool inProcess = false;

        private void Reader_FrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (!inProcess) {
                inProcess = true;
                count++;
                this.Message = count.ToString();
                
                MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

                if (multiSourceFrame != null)
                {
                    using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
                    {
                        using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
                        {
                            if (depthFrame != null)
                            {
                                FrameDescription depthFrameDescription = depthFrame.FrameDescription;

                                if ((depthWidth * depthHeight) == this.depthFrameData.Length)
                                {
                                    depthFrame.CopyFrameDataToArray(this.depthFrameData);
                                }

                                if (!depthSet)
                                {
                                    depthSet = true;
                                    maxDepth = depthFrame.DepthMaxReliableDistance;
                                    minDepth = depthFrame.DepthMinReliableDistance;
                                }
                            }
                            
                            if (colorFrame != null)
                            {
                                FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                                if ((colorWidth * colorHeight * bytesPerColor) == this.colorFrameData.Length)
                                {
                                    colorFrame.CopyConvertedFrameDataToArray(this.colorFrameData, ColorImageFormat.Rgba);
                                }
                            }
                        }
                    }
                }

                if (filteringEnabled)
                {
                    if (outerBandThreshold > 0 && filteredSmoothing != null)
                    {
                        depthFrameData = filteredSmoothing.CreateFilteredDepthArray(depthFrameData);
                    }
                }
                if (smoothingEnabled)
                {
                    // Smooth the depth buffer
                    if (smoothBufferSize > 0 && averagedSmoothing != null)
                    {
                        depthFrameData = averagedSmoothing.CreateAverageDepthArray(depthFrameData);
                    }
                }

                this.coordinateMapper.MapDepthFrameToColorSpace(this.depthFrameData, this.colorPoints);
                this.coordinateMapper.MapDepthFrameToCameraSpace(this.depthFrameData, this.cameraPoints);
                               
                inProcess = false;
                
            }
        }

        #region DepthUserOptions
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            var header = new ToolStripMenuItem("Depth Settings");
            header.Enabled = false;
            menu.Items.Add(header);

            var item = new ToolStripTextBox("Minimum Depth");
            item.Name = "Minimum Depth";
            item.ToolTipText = "The minimum depth for a point to be processed.";
            item.Text = minDepth.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            item = new ToolStripTextBox("Maximum Depth");
            item.Name = "Maximum Depth";
            item.ToolTipText = "The maximum depth for a point to be processed.";
            item.Text = maxDepth.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            var item0 = Menu_AppendItem(menu, "Smoothing", Menu_OptionToggled, true, smoothingEnabled);
            item0.ToolTipText = "Toggles smoothing on/off.";

            item = new ToolStripTextBox("Smooth Buffer");
            item.Name = "Smooth Buffer";
            item.ToolTipText = "The number of frames to smooth the depth buffer.";
            item.Text = smoothBufferSize.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            /*
            var item1 = Menu_AppendItem(menu, "Filtering", Menu_OptionToggled, true, filteringEnabled);
            item1.ToolTipText = "Toggles filtering on/off.";

            item = new ToolStripTextBox("Inner Band");
            item.Name = "Inner Band";
            item.ToolTipText = "The inner band for for filtering.";
            item.Text = innerBandThreshold.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            item = new ToolStripTextBox("Outer Band");
            item.Name = "Outer Band";
            item.ToolTipText = "The outer band for filtering.";
            item.Text = outerBandThreshold.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());
            */
            header = new ToolStripMenuItem("Cropping");
            header.Enabled = false;
            menu.Items.Add(header);

            item = new ToolStripTextBox("Cropping");
            item.Name = "Cropping";
            item.ToolTipText = "The number of pixels to crop from the edge of the kinect.";
            item.Text = cropping.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            header = new ToolStripMenuItem("Detail");
            header.Enabled = false;
            menu.Items.Add(header);

            item = new ToolStripTextBox("Detail");
            item.Name = "Detail";
            item.ToolTipText = "The grid size (in pixels) for generating the point cloud.";
            item.Text = detail.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

			menu.Items.Add(new ToolStripSeparator());
        }

        private void Menu_OptionToggled(object sender, EventArgs args)
        {
            ToolStripMenuItem item = (ToolStripMenuItem) sender;
            if (item.Text == "Filtering")
            {
                filteringEnabled = !filteringEnabled;
                item.Checked = filteringEnabled;
            } else if (item.Text == "Smoothing")
            {
                smoothingEnabled = !smoothingEnabled;
                item.Checked = smoothingEnabled;
            }
        }

        private void Menu_TextChanged(object sender, EventArgs args)
        {
            //RecordUndoEvent("MenuChanged");
            //Params.OnParametersChanged();

            ToolStripTextBox txtBox = (ToolStripTextBox)sender;
            if (txtBox.Name == "Maximum Depth")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    depthSet = true;
                    maxDepth = result;
                }
            }
            else if (txtBox.Name == "Minimum Depth")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    depthSet = true;
                    minDepth = result;
                }
            }
            else if (txtBox.Name == "Smooth Buffer")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result >= 0)
                    {
                        if (result > AveragedSmoothing.MaxAverageFrameCount) result = AveragedSmoothing.MaxAverageFrameCount;
                        smoothBufferSize = result;
                        if (averagedSmoothing != null)
                        {
                            averagedSmoothing.AverageFrameCount = smoothBufferSize;
                        }
                    } else
                    {
                        txtBox.Text = smoothBufferSize.ToString();
                    }
                }
            }
            else if (txtBox.Name == "Inner Band")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    innerBandThreshold = result;
                    if (filteredSmoothing != null)
                    {
                        filteredSmoothing.InnerBandThreshold = innerBandThreshold;
                    }
                }
            }
            else if (txtBox.Name == "Outer Band")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    outerBandThreshold = result;
                    if (filteredSmoothing != null)
                    {
                        filteredSmoothing.OuterBandThreshold = outerBandThreshold;
                    }
                }
            }
            else if (txtBox.Name == "Cropping")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result < 0) result = 0;
                    cropping = result;
                }
            }
            else if (txtBox.Name == "Detail")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result < 1) result = 1;
                    if (result > 20) result = 20;
                    detail = result;
                }
            }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetDouble("maxDepth", maxDepth);
            writer.SetDouble("minDepth", minDepth);
            writer.SetBoolean("depthSet", depthSet);
            writer.SetInt32("smoothBufferSize", smoothBufferSize);
            writer.SetInt32("outerBandThreshold", outerBandThreshold);
            writer.SetInt32("innerBandThreshold", innerBandThreshold);
            writer.SetBoolean("filteringEnabled", filteringEnabled);
            writer.SetBoolean("smoothingEnabled", smoothingEnabled);
            writer.SetInt32("cropping", cropping);
            writer.SetInt32("detail", detail);
            return base.Write(writer);
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            maxDepth = (ushort) reader.GetDouble("maxDepth");
            minDepth = (ushort)reader.GetDouble("minDepth");
            depthSet = reader.GetBoolean("depthSet");
            smoothBufferSize = reader.GetInt32("smoothBufferSize");
            outerBandThreshold = reader.GetInt32("outerBandThreshold");
            innerBandThreshold = reader.GetInt32("innerBandThreshold");
            filteringEnabled = reader.GetBoolean("filteringEnabled");
            smoothingEnabled = reader.GetBoolean("smoothingEnabled");
            cropping = reader.GetInt32("cropping");
            detail = reader.GetInt32("detail");
            return base.Read(reader);
        }

        #endregion

    }
}
