﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Windows.Forms;
using OpenNIWrapper;
using TarsierVision.Processing;

namespace Tarsier.Kinect
{
    public class OpenNIComponent : GH_Component
    {
        public OpenNIComponent()
          : base("OpenNI2 Depth Reader", "NI2",
              "Reads data from an OpenNI2 device.",
              "Tarsier", "Vision")
        {
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Enable", "E", "Enable this component. This will turn the kinect on and begin monitoring.", GH_ParamAccess.item, false);
            pManager.AddBooleanParameter("Release", "R", "Release the latest frame. Connect a timer to enable iterative release.", GH_ParamAccess.item, false);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Point Cloud", "C", "The depth buffer as a point cloud.", GH_ParamAccess.item);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return TarsierVision.Properties.Resources.icon_openni2; }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{64094844-3B2D-4C29-8323-11C68FF8D1BE}"); }
        }

        private OpenNIWrapper.Device sensor;
        //private VideoStream colorStream = null;
        private VideoStream depthStream = null;
        //private Bitmap depthBitmap = null;
        private Object depthLock = new Object();
        private ushort[] depthData = null;
        //private Bitmap colorBitmap = null;

        private PointCloud currentPC = new PointCloud();

        bool depthSet = false;
        int minDepth = 0;
        int maxDepth = 8000;
        int count;

        int depthWidth;
        int depthHeight;


        int prevCount = -1;


        private bool filteringEnabled = false;
        private bool smoothingEnabled = true;

        AveragedSmoothing averagedSmoothing;
        private int smoothBufferSize = 10;

        FilteredSmoothing filteredSmoothing;
        private int innerBandThreshold = 1;
        private int outerBandThreshold = 3;

        private int cropping = 0;
        private int detail = 1;

        private bool open = false;

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool enable = false;
            bool release = false;
            if (!DA.GetData("Enable", ref enable)) return;
            if (!DA.GetData("Release", ref release)) return;

            
            if (enable && !open)
            {
                
                OpenNI.Status status = OpenNI.Initialize();
                try {
                    sensor = Device.Open(null, "lr");
                } catch
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, OpenNI.LastError);
                }

                //Rhino.RhinoApp.WriteLine("Status: " + status);
                //Rhino.RhinoApp.WriteLine("Sensor: " + sensor);

                if (sensor != null) {
                    if (sensor.HasSensor(Device.SensorType.Depth))
                    {
                        depthStream = sensor.CreateVideoStream(Device.SensorType.Depth);
                        depthStream.Start();
                        depthStream.OnNewFrame += DepthFrameArrived;
                        if (!depthSet)
                        {
                            minDepth = depthStream.MinPixelValue;
                            maxDepth = depthStream.MaxPixelValue;
                        }
                        
                    }
                    if (sensor.HasSensor(Device.SensorType.Color))
                    {
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Color streams not yet implemented.");
                        //colorStream = sensor.CreateVideoStream(Device.SensorType.Color);
                    }

                    open = true;
                    prevCount = -1;
                    count = 0;
                    

                }
                
            }
            else if (!enable && sensor != null && open)
            {
                open = false;
                
                if (sensor.HasSensor(Device.SensorType.Depth))
                {
                    depthStream.Stop();
                }

                if (sensor.HasSensor(Device.SensorType.Color))
                {
                    //colorStream.Stop();
                }

                sensor.Close();
                depthStream.OnNewFrame -= DepthFrameArrived;
                sensor = null;
                OpenNI.Shutdown();
                
            }

            if (enable && sensor != null && open && release && depthData != null)
            {
                if (count > prevCount)
                {
                    currentPC = new PointCloud();
                    prevCount = count;

                    lock(depthLock)
                    {
                        for (int x=0+cropping; x<depthWidth-cropping; x+=detail)
                        {
                            for (int y=0+ cropping; y<depthHeight- cropping; y += detail)
                            {
                                ushort depthVal = depthData[y * depthWidth + x];

                                if (depthVal > minDepth && depthVal < maxDepth)
                                {

                                    float wX;
                                    float wY;
                                    float wZ;

                                    CoordinateConverter.ConvertDepthToWorld(depthStream, x, y, depthVal, out wX, out wY, out wZ);
                                    currentPC.Add(new Point3d(wX, wY, wZ));
                                }
                            }
                        }
                    }
                }
                DA.SetData("Point Cloud", currentPC);
            }
        }

        private double remap(double current, double oldMin, double oldMax, double newMin, double newMax)
        {
            return (current - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
        }

        private void DepthFrameArrived(VideoStream stream)
        {
            // Lock the stream
            
            using (VideoFrameRef frame = stream.ReadFrame())
            {
                if (frame.IsValid)
                {
                    lock(depthLock)
                    {
                        //depthBitmap = frame.ToBitmap(VideoFrameRef.CopyBitmapOptions.DepthFillRigthBlack | VideoFrameRef.CopyBitmapOptions.Force24BitRgb);
                        if (depthData == null) depthData = new ushort[frame.FrameSize.Width * frame.FrameSize.Height];
                        depthWidth = frame.FrameSize.Width;
                        depthHeight = frame.FrameSize.Height;


                        if (filteredSmoothing == null) filteredSmoothing = new FilteredSmoothing(innerBandThreshold, outerBandThreshold, depthWidth, depthHeight);
                        if (averagedSmoothing == null) averagedSmoothing = new AveragedSmoothing(this.smoothBufferSize, depthWidth, depthHeight);

                        ProcessFrame(frame.Data, frame.DataSize, frame.DataStrideBytes);
                    }
                    count++;
                    this.Message = count.ToString();
                }


                if (filteringEnabled)
                {
                    if (outerBandThreshold > 0 && filteredSmoothing != null)
                    {
                        depthData = filteredSmoothing.CreateFilteredDepthArray(depthData);
                    }
                }
                if (smoothingEnabled)
                {
                    // Smooth the depth buffer
                    if (smoothBufferSize > 0 && averagedSmoothing != null)
                    {
                        depthData = averagedSmoothing.CreateAverageDepthArray(depthData);
                    }
                }
            }
        }

        public unsafe void ProcessFrame(IntPtr data, int size, int dataStrideBytes)
        {

            ushort* d = (ushort*) data;

            for (int i=0; i<size/2; i++)
            {
                ushort val = (ushort)d[i];
                depthData[i] = val;
            }
        }
        

        #region DepthUserOptions
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            var header = new ToolStripMenuItem("Depth Settings");
            header.Enabled = false;
            menu.Items.Add(header);

            var item = new ToolStripTextBox("Minimum Depth");
            item.Name = "Minimum Depth";
            item.ToolTipText = "The minimum depth for a point to be processed.";
            item.Text = minDepth.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            item = new ToolStripTextBox("Maximum Depth");
            item.Name = "Maximum Depth";
            item.ToolTipText = "The maximum depth for a point to be processed.";
            item.Text = maxDepth.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            var item0 = Menu_AppendItem(menu, "Smoothing", Menu_OptionToggled, true, smoothingEnabled);
            item0.ToolTipText = "Toggles smoothing on/off.";

            item = new ToolStripTextBox("Smooth Buffer");
            item.Name = "Smooth Buffer";
            item.ToolTipText = "The number of frames to smooth the depth buffer.";
            item.Text = smoothBufferSize.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            /*
            var item1 = Menu_AppendItem(menu, "Filtering", Menu_OptionToggled, true, filteringEnabled);
            item1.ToolTipText = "Toggles filtering on/off.";

            item = new ToolStripTextBox("Inner Band");
            item.Name = "Inner Band";
            item.ToolTipText = "The inner band for for filtering.";
            item.Text = innerBandThreshold.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            item = new ToolStripTextBox("Outer Band");
            item.Name = "Outer Band";
            item.ToolTipText = "The outer band for filtering.";
            item.Text = outerBandThreshold.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());
            */
            header = new ToolStripMenuItem("Cropping");
            header.Enabled = false;
            menu.Items.Add(header);

            item = new ToolStripTextBox("Cropping");
            item.Name = "Cropping";
            item.ToolTipText = "The number of pixels to crop from the edge of the kinect.";
            item.Text = cropping.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);

            menu.Items.Add(new ToolStripSeparator());

            header = new ToolStripMenuItem("Detail");
            header.Enabled = false;
            menu.Items.Add(header);

            item = new ToolStripTextBox("Detail");
            item.Name = "Detail";
            item.ToolTipText = "The grid size (in pixels) for generating the point cloud.";
            item.Text = detail.ToString();
            item.TextChanged += Menu_TextChanged;
            item.BorderStyle = BorderStyle.FixedSingle;
            menu.Items.Add(item);
        }


        private void Menu_OptionToggled(object sender, EventArgs args)
        {
            ToolStripMenuItem item = (ToolStripMenuItem) sender;
            if (item.Text == "Filtering")
            {
                filteringEnabled = !filteringEnabled;
                item.Checked = filteringEnabled;
            } else if (item.Text == "Smoothing")
            {
                smoothingEnabled = !smoothingEnabled;
                item.Checked = smoothingEnabled;
            }
        }

        private void Menu_TextChanged(object sender, EventArgs args)
        {
            //RecordUndoEvent("MenuChanged");
            //Params.OnParametersChanged();

            ToolStripTextBox txtBox = (ToolStripTextBox)sender;
            if (txtBox.Name == "Maximum Depth")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    depthSet = true;
                    maxDepth = result;
                }
            }
            else if (txtBox.Name == "Minimum Depth")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    depthSet = true;
                    minDepth = result;
                }
            }
            else if (txtBox.Name == "Smooth Buffer")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result >= 0)
                    {
                        if (result > AveragedSmoothing.MaxAverageFrameCount) result = AveragedSmoothing.MaxAverageFrameCount;
                        smoothBufferSize = result;
                        if (averagedSmoothing != null)
                       {
                           averagedSmoothing.AverageFrameCount = smoothBufferSize;
                       }
                    } else
                    {
                        txtBox.Text = smoothBufferSize.ToString();
                    }
                }
            }
            else if (txtBox.Name == "Inner Band")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    innerBandThreshold = result;
                    if (filteredSmoothing != null)
                    {
                        filteredSmoothing.InnerBandThreshold = innerBandThreshold;
                    }
                }
            }
            else if (txtBox.Name == "Outer Band")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    outerBandThreshold = result;
                    if (filteredSmoothing != null)
                    {
                        filteredSmoothing.OuterBandThreshold = outerBandThreshold;
                    }
                }
            }
            else if (txtBox.Name == "Cropping")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result < 0) result = 0;
                    cropping = result;
                }
            }
            else if (txtBox.Name == "Detail")
            {
                int result;
                if (int.TryParse(txtBox.Text, out result))
                {
                    if (result < 1) result = 1;
                    if (result > 20) result = 20;
                    detail = result;
                }
            }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetDouble("maxDepth", maxDepth);
            writer.SetDouble("minDepth", minDepth);
            writer.SetBoolean("depthSet", depthSet);
            writer.SetInt32("smoothBufferSize", smoothBufferSize);
            writer.SetInt32("outerBandThreshold", outerBandThreshold);
            writer.SetInt32("innerBandThreshold", innerBandThreshold);
            writer.SetBoolean("filteringEnabled", filteringEnabled);
            writer.SetBoolean("smoothingEnabled", smoothingEnabled);
            writer.SetInt32("cropping", cropping);
            writer.SetInt32("detail", detail);
            return base.Write(writer);
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            maxDepth = (ushort) reader.GetDouble("maxDepth");
            minDepth = (ushort)reader.GetDouble("minDepth");
            depthSet = reader.GetBoolean("depthSet");
            smoothBufferSize = reader.GetInt32("smoothBufferSize");
            outerBandThreshold = reader.GetInt32("outerBandThreshold");
            innerBandThreshold = reader.GetInt32("innerBandThreshold");
            filteringEnabled = reader.GetBoolean("filteringEnabled");
            smoothingEnabled = reader.GetBoolean("smoothingEnabled");
            cropping = reader.GetInt32("cropping");
            detail = reader.GetInt32("detail");
            return base.Read(reader);
        }

        #endregion
    

    }
}
